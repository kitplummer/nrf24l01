defmodule Rf24l01Test do
  use ExUnit.Case
  alias Wafer.Driver.Circuits.GPIO
  alias Wafer.Driver.Circuits.SPI

  test "greets the world" do
    {:ok, ce_pin_17} = GPIO.acquire(pin: 17, direction: :out)    
    {:ok, csn_pin_25} = GPIO.acquire(pin: 25, direction: :out)
    {:ok, spi} = SPI.acquire(speed_hz: 8000000, bus_name: "spidev0.0")
    {:ok, conn} = Rf24l01.acquire(conn: spi, ce_pin: ce_pin_17, csn_pin: csn_pin_25)

    Rf24l01.init(conn)
    # {:ok, <<instruction, data>>, conn} = Wafer.Chip.read_register(%Rf24l01{conn: conn}, 0x00, 1)
    # assert 12 == data
    address = <<0xF0F0F0F0D2>>
    IO.inspect address, label: "RX Address"
    Rf24l01.open_rx_pipe(conn, 0, address)
    Rf24l01.start_listening(conn)

    if Rf24l01.any(conn) > 0 do
      IO.puts "there be data"
      data = Rf24l01.recv(conn)
      IO.inspect data, label: "DATA!"
    end

    #assert conn == :ok 
  end
end
