defmodule Rf24l01.Application do
  use Application

  @impl true
  def start(_type, _args) do
    # Although we don't use the supervisor name below directly,
    # it can be useful when debugging or introspecting the system.
    Rf24l01.Supervisor.start_link(name: Rf24l01.Supervisor)
  end
end