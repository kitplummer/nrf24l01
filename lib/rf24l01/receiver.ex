defmodule Rf24l01.Receiver do
  alias Wafer.Driver.Circuits.GPIO
  alias Wafer.Driver.Circuits.SPI
  use GenServer
  @timedelay 2000

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init([]) do
    :random.seed(:os.timestamp)
    time = :random.uniform
    {:ok, ce_pin_17} = GPIO.acquire(pin: 17, direction: :out)    
    {:ok, csn_pin_25} = GPIO.acquire(pin: 25, direction: :out)
    {:ok, spi} = SPI.acquire(speed_hz: 8000000, bus_name: "spidev0.0")
    {:ok, conn} = Rf24l01.acquire(conn: spi, ce_pin: ce_pin_17, csn_pin: csn_pin_25)

    Rf24l01.init(conn)
    data = get_data(conn)
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    state = %{time: time, conn: conn, data: data, timer: timer_ref}
    {:ok, state}
  end

  def get_state() do
    GenServer.call(Process.whereis(__MODULE__), :get_state)
  end

  def handle_call(:get_state, _from, state) do
    IO.puts "Getting State"
    return = Map.take(state, [:time, :data])
    {:reply, return, state} 
  end

  def handle_info(:tick, state) do
    new_state = run(state)
    timer_ref = Process.send_after(self(), :tick, @timedelay)
    {:noreply, %{new_state | timer: timer_ref}}
  end

  defp run(state) do
    new_time = state.time + :random.uniform/12
    new_data = get_data(state.conn)
    new_state = %{state | time: new_time, data: new_data}
    IO.inspect new_data, label: "New Data!"
    new_state
  end

  defp get_data(conn) do

    # {:ok, <<instruction, data>>, conn} = Wafer.Chip.read_register(%Rf24l01{conn: conn}, 0x00, 1)
    # assert 12 == data
    address = <<0xF0F0F0F0D2>>
    IO.inspect address, label: "RX Address"
    Rf24l01.open_rx_pipe(conn, 0, address)
    Rf24l01.start_listening(conn)
    data =
      if Rf24l01.any(conn) > 0 do
        IO.puts "there be data"
        Rf24l01.recv(conn)
      end

    data
  end
end