defimpl Wafer.Chip, for: Rf24l01 do
  alias Wafer.SPI

  def read_register(%Rf24l01{conn: conn} = device, address, bytes) do
    payload_bytes = bytes * 8
    instruction = <<0::integer-size(3), address::integer-size(5), 0::integer-size(payload_bytes)>>
    case SPI.transfer(conn, instruction) do
      {:ok, <<_, data::binary>>, conn} -> 
        {:ok, data, %{device | conn: conn}}
      {:error, reason} -> {:error, reason}
    end
  end

  def swap_register(%Rf24l01{} = device, address, data) do
    with {:ok, data, device} <- read_register(device, address, byte_size(data)),
         {:ok, device} <- write_register(device, address, data) do
      {:ok, data, device}
    else
      {:error, reason} -> {:error, reason}
    end
  end

  def write_register(%Rf24l01{conn: conn} = device, address, data) do
    instruction = <<1::integer-size(3), address::integer-size(5), data::binary>>
    case SPI.transfer(conn, instruction) do
      {:ok, <<_, data>>, conn} -> 
        {:ok, <<data>>, %{device | conn: conn}}
      {:ok, <<_, b0, b1, b2, b3, b4>>, conn } -> 
        {:ok, b0, b1, b2, b3, b4, %{device | conn: conn}}
      {:error, reason} -> {:error, reason}
    end
  end
end
