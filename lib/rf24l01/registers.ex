defmodule Rf24l01.Registers do
  use Wafer.Registers

  defregister(:config, 0x00, :rw, 1)
  defregister(:en_aa, 0x01, :rw, 1)
  defregister(:en_rx, 0x02, :rw, 1)
  defregister(:setup_aw, 0x03, :rw, 1)
  defregister(:setup_retr, 0x04, :rw, 1)
  defregister(:rf_ch, 0x05, :rw, 1)
  defregister(:rf_setup, 0x06, :rw, 1)
  defregister(:rx_addr_0, 0x0a, :rw, 5) # This includes the status bit, which has to be truncated
  defregister(:rx_addr_1, 0x0b, :rw, 5) # This includes the status bit, which has to be truncated
  defregister(:rx_addr_2, 0x0c, :rw, 1)
  defregister(:rx_addr_3, 0x0d, :rw, 1)
  defregister(:rx_addr_4, 0x0e, :rw, 1)
  defregister(:rx_addr_5, 0x0f, :rw, 1)
  defregister(:rx_pw_0, 0x11, :rw, 1)
  defregister(:rx_pw_1, 0x12, :rw, 1)
  defregister(:rx_pw_2, 0x13, :rw, 1)
  defregister(:rx_pw_3, 0x14, :rw, 1)
  defregister(:rx_pw_4, 0x15, :rw, 1)
  defregister(:rx_pw_5, 0x16, :rw, 1)
  defregister(:fifo, 0x17, :rw, 1)
  defregister(:dynpd, 0x1c, :rw, 1)
  defregister(:feature, 0x1d, :rw, 1)
  defregister(:tx_addr, 0x10, :rw, 5)
  defregister(:status, 0x07, :rw, 1)
  defregister(:any, 0x60, :ro, 1)

end


