defmodule Rf24l01 do

  @derive [Wafer.SPI]
  defstruct ~w[conn ce_pin csn_pin state]a
  @behaviour Wafer.Conn
  alias Rf24l01.Registers
  alias Wafer.Conn
  alias Wafer.GPIO
  use Bitwise
  require Logger

  @type t :: %Rf24l01{conn: Conn.t(), ce_pin: Conn.t(), csn_pin: Conn.t()}
  @type acquire_options :: [acquire_option]
  @type acquire_option :: {:conn, Conn.t(), ce_pin: Conn.t(), csn_pin: Conn.t()}

  @payload_length 32

  @impl Wafer.Conn
  def acquire(options) do
    with {:ok, conn} <- Keyword.fetch(options, :conn),
      ce_pin <- Keyword.get(options, :ce_pin),
      csn_pin <- Keyword.get(options, :csn_pin) do
        {:ok, %Rf24l01{conn: conn, ce_pin: ce_pin, csn_pin: csn_pin, state: :acquired}}
    else
      :error -> {:error, "`Rf24.acquire/1` requires the `conn` option."}
      {:error, reason} -> {:error, reason}
    end
  end

  def init(conn,
    channel \\ 76,
    payload_length \\ 32,
    address_length \\5,
    ard \\ 1500,
    arc \\ 3,
    crc \\ 2,
    data_rate \\ 1,
    pa_level \\ 0,
    dynamic_payloads \\ true,
    auto_ack \\ true,
    ask_no_ack \\ true,
    ack \\ false,
    irq_dr \\ true,
    irq_ds \\ true,
    irq_df \\ true)
  do
    
    @payload_length = payload_length

    set_ce_low(conn)
    init_config =
      (bool_to_int(not(irq_dr)) <<< 6)
      |> bor((bool_to_int(not(irq_ds)) <<< 5))
      |> bor((bool_to_int(not(irq_df)) <<< 4))
      |> bor((crc + 1) <<< 2)
      |> bor(2)
    
    {:ok, <<write_data>>, _} = Registers.write_config(conn, <<init_config>>)
    {:ok, read_data} =
      case Registers.read_config(conn) do
        {:ok, <<data>>, _} -> {:ok, data}
        {:error, error} -> {:error, error}
      end

    if write_data == read_data, do: Logger.info("READ CONFIG CORRECTLY")

    tx? = band(read_data, 3)

    case tx? == 2 do
      true -> power = false
      false -> {:error, "unable to init rf24l01"}
    end

    pipes = get_pipes(conn)
    #IO.inspect pipes, label: "PIPES"

    tx_addr = get_tx_addr(conn)
    #IO.inspect tx_addr, label: "TX_ADDR"

    ## config the SETUP_RETR register
    setup_retr = 
      if 250 <= ard and ard <= 4000 and rem(ard, 250) == 0 and 0 <= arc and arc <= 15 do
        (round((ard - 250) / 250) <<< 4) || arc
      else
        ## ERROR and exit
      end

    # configure the RF_SETUP register
    if Enum.member?([1, 2, 250], data_rate) and Enum.member?([-18, -12, -6, 0], pa_level) do
      data_rate =
        cond do
          data_rate == 1 -> 0
          data_rate == 2 -> 8
          data_rate == 250 -> 0x20
        end
      
      pa_level = (3 - round(pa_level / -6)) * 2
      rf_setup = bor(data_rate, pa_level)

      {:ok, _, _} = Registers.write_rf_setup(conn, <<rf_setup>>)
      {:ok, read_rf_setup} =
        case Registers.read_rf_setup(conn) do
          {:ok, <<rf_setup_reg>>, _} -> {:ok, rf_setup_reg}
          {:error, error} -> {:error, error}
        end
    else
      ## ERROR and exit
    end

    # setup open/close pipe status
    {:ok, _, _} = Registers.write_en_rx(conn, <<0>>)
    {:ok, read_en_rx} =
        case Registers.read_en_rx(conn) do
          {:ok, <<en_rx>>, _} -> {:ok, en_rx}
          {:error, error} -> {:error, error}
        end
    
    ## DYNAMIC PAYLOADS
    dyn_pl = if dynamic_payloads, do: 0x3F, else: 0
    {:ok, _, _} = Registers.write_dynpd(conn, <<dyn_pl>>)
    {:ok, <<read_dynpd>>, _} = Registers.read_dynpd(conn)

    ## AUTO_ACK
    aa = if auto_ack, do: 0x3F, else: 0
    {:ok, _, _} = Registers.write_en_aa(conn, <<aa>>)

    ## FEATURES
    ack =  if auto_ack and dynamic_payloads, do: ack, else: false
    features = 
      (bool_to_int(dynamic_payloads) <<< 2)
      |> bor(bool_to_int(ack) <<< 1)
      |> bor(bool_to_int(ask_no_ack))
    Registers.write_feature(conn, <<features>>)
    {:ok, <<read_feature>>, _} = Registers.read_feature(conn)

    ## SETUP_RETR
    Registers.write_setup_retr(conn, <<setup_retr>>)
    {:ok, <<read_setup_retr>>, _} = Registers.read_setup_retr(conn)

    ## SETUP PIPES
    write_pipes(conn, pipes)

    ## SETUP TX_ADDR
    Registers.write_tx_addr(conn, tx_addr)

    ## SETUP SETUP_AW
    Registers.write_setup_aw(conn, <<address_length>>)

    ## SETUP RF_CH
    Registers.write_rf_ch(conn, <<channel>>)

    ## ENABLE RX MODE
    Registers.write_config(conn, <<init_config ||| 1>> )
    {:ok, <<read_config>>, _} = Registers.read_config(conn) 
    :timer.sleep(15) # wait for transition on mode
    
    ## Flush RX
    Registers.write_fifo(conn, <<0xE2>>)

    ## POWER DOWN + TX MODE

    Registers.write_config(conn, <<init_config &&& 0xC>>)
    {:ok, <<read_config>>, _} = Registers.read_config(conn) 
    :timer.sleep(15) # wait for transition on mode

    ## Flush TX
    Registers.write_fifo(conn, <<0xE1>>)

    ## Clear STATUS register
    clear_status_flags(conn)

  end

  def open_rx_pipe(conn, pipe_number, address) do
    case pipe_number do
      0 -> Registers.write_rx_addr_0(conn, address)
      1 -> Registers.write_rx_addr_1(conn, address)
      2 -> Registers.write_rx_addr_2(conn, address)
      3 -> Registers.write_rx_addr_3(conn, address)
      4 -> Registers.write_rx_addr_4(conn, address)
      5 -> Registers.write_rx_addr_5(conn, address)
    end

    {:ok, <<open_pipes>>, _} = Registers.read_en_rx(conn)
    open_pipes = open_pipes ||| (1 <<< pipe_number)
    case pipe_number do
      0 -> Registers.write_rx_pw_0(conn, @payload_length)
      1 -> Registers.write_rx_pw_1(conn, @payload_length)
      2 -> Registers.write_rx_pw_2(conn, @payload_length)
      3 -> Registers.write_rx_pw_3(conn, @payload_length)
      4 -> Registers.write_rx_pw_4(conn, @payload_length)
      5 -> Registers.write_rx_pw_5(conn, @payload_length)
    end

  end

  def start_listening(conn) do
    # Power Down or standby-I mode
    set_ce_low(conn)
    {:ok, <<config>>, conn} = Registers.read_config(conn)
    Registers.write_config(conn, <<config &&& 0xFC ||| 3>>)
    :timer.sleep(1) # wait for transition on mode
    {:ok, <<config>>, conn} = Registers.read_config(conn)
    flush_rx(conn)
    clear_status_flags(conn, true, false, false)

    set_ce_high(conn)
    :timer.sleep(1)
    
  end
  
  def any(conn) do
    {:ok, <<payload_size>>, conn} = Registers.read_any(conn)
    IO.inspect payload_size, label: "Payload Length"
    payload_size
  end

  def recv(conn) do
    curr_pl_size  = any(conn)
    {:ok, result, conn} = Wafer.Chip.read_register(%Rf24l01{conn: conn}, 0x61, curr_pl_size)
    IO.inspect result, label: "RX"
    clear_status_flags(conn, true, false, false)
    result
  end

  defp flush_rx(conn) do
    Registers.write_fifo(conn, <<0xE2>>)
  end

  defp flush_tx(conn) do
    Registers.write_fifo(conn, <<0xE1>>)
  end
 
  defp clear_status_flags(conn, data_recv \\ true, data_sent \\ true, data_fail \\ true) do
    instruction = 
      (bool_to_int(data_recv) <<< 6)
      |> bor((bool_to_int(data_sent) <<< 5))
      |> bor((bool_to_int(data_fail) <<< 4))
    Registers.write_status(conn, <<instruction>>)
  end

  defp set_ce_low(conn), do: GPIO.write(conn.ce_pin, 0)
  defp set_ce_high(conn), do: GPIO.write(conn.ce_pin, 1)

  defp write_pipes(conn, pipes) do
    Registers.write_rx_addr_0(conn, Enum.at(pipes,0))
    Registers.write_rx_addr_1(conn, Enum.at(pipes,1))
    Registers.write_rx_addr_2(conn, Enum.at(pipes,2))
    Registers.write_rx_addr_3(conn, Enum.at(pipes,3))
    Registers.write_rx_addr_4(conn, Enum.at(pipes,4))
    Registers.write_rx_addr_5(conn, Enum.at(pipes,5))
  end

  defp get_pipes(conn) do
    ## Meta!!
    {:ok, p0, _} = Registers.read_rx_addr_0(conn)
    p0 = binary_part(p0, 1, byte_size(p0) - 1) 
    {:ok, p1, _} = Registers.read_rx_addr_1(conn)
    p1 = binary_part(p1, 1, byte_size(p1) - 1) 
    {:ok, p2, _} = Registers.read_rx_addr_2(conn)
    p2 = binary_part(p2, 1, byte_size(p2) - 1) 
    {:ok, p3, _} = Registers.read_rx_addr_3(conn)
    p3 = binary_part(p3, 1, byte_size(p3) - 1) 
    {:ok, p4, _} = Registers.read_rx_addr_4(conn)
    p4 = binary_part(p4, 1, byte_size(p4) - 1) 
    {:ok, p5, _} = Registers.read_rx_addr_5(conn)
    p5 = binary_part(p5, 1, byte_size(p5) - 1) 

    [p0, p1, p2, p3, p4, p5]
  end

  defp get_tx_addr(conn) do
    {:ok, tx, _} = Registers.read_tx_addr(conn)
    binary_part(tx, 1, byte_size(tx) - 1) 
  end

  defp bool_to_int(bool) do
    if bool, do: 1, else: 0
  end

end
